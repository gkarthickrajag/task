const express = require('express')
const router = express.Router()
const { check } = require('express-validator')
const postController = require('../controller/post.controller')

router.post('/addPost', [
    check('title').escape(),
    check('description').escape(),
    check('image').escape()
], postController.addPost)

router.get('/getPosts', postController.getPosts)

router.post('/searchPosts', postController.searchPosts)

router.post('/filterByTag', postController.filterByTag )

router.post('/uploadImage', postController.uploadImage )

module.exports = router
