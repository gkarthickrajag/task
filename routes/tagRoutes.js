const express = require('express')
const router = express.Router()
const { check } = require('express-validator')
const tagController = require('../controller/tag.controller')

router.post('/addtag', [
    check('tagName').escape(),
    check('postId').escape(),
], tagController.addTag)

module.exports = router
