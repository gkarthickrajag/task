const mongoose = require('mongoose')
const config = require('../config')
const postSchema = new mongoose.Schema({
  title: {
    type: String,
  },
  description: {
    type: String
  },
  imageName: {
    type: String
  },
  active: {
    type: Boolean
  },
  tags:[{ type: String }]
},
{
  timestamps: true,
  strict: true
})

var collectionName = 'posts' 
postSchema.virtual('post_id').get(function() { return this._id; });
postSchema.virtual('image').get(function() { return config.baseUrl+this.image; });
postSchema.set('toJSON', {
  virtuals: true,
  transform: (doc, ret, options) => {
      delete ret.__v;delete ret._id;delete ret.id;delete ret.createdAt;delete ret.updatedAt;
      delete ret.imageName;
  },
});
module.exports = mongoose.model('posts', postSchema,collectionName)
