const mongoose = require('mongoose')

const tagSchema = new mongoose.Schema({
  tagName: {
    type: String,
  }
},
{
  timestamps: true,
  strict: true
})

var collectionName = 'tags' 
tagSchema.virtual('tag_id').get(function() { return this._id; });
tagSchema.set('toJSON', {
  virtuals: true,
  transform: (doc, ret, options) => {
      delete ret.__v;delete ret._id;delete ret.id;delete ret.createdAt;delete ret.updatedAt;
      delete ret.image_url;
  },
});
module.exports = mongoose.model('tags', tagSchema,collectionName)
