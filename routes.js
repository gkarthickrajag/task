const PostRouter = require('./routes/postRoutes')
const TagRouter = require('./routes/tagRoutes')

module.exports = function(app) {
    /*Parent Routes */
    app.use('/api/v1/post', PostRouter)
    app.use('/api/v1/tag', TagRouter)
}