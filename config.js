module.exports = {

    server: {
        port: '6000'
    },
    baseUrl: 'http://localhost:6000/',
    database:{ 
        url:'mongodb://localhost:27017/postDB'
    },
    AWS_S3_ACCESS_KEY_ID:'',
    AWS_S3_SECRET_ACCESS_KEY:'',
    AWS_S3_BUCKET_NAME:''
}