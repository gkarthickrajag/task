const { validationResult } = require('express-validator')
const TagModel = require('../models/tagsModel')
const PostModel = require('../models/postModel')

// add tags
exports.addTag = async (req, res) => {
    // validating the i/p parameters
    const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ success: false, message: 'Invalid Inputs', errors: errors.array(), code: 'INVALID_INPUT' })
  }
  try {
  const tagJson = {
    tagName: req.body.tagName,
  }
  //instance created to save posts
  const tags = new TagModel(postJson)
   // save posts
   tags.save(async function (err, saved){
    if(err) {
        res.send({ status: false, message: err.message})
    } else if (saved) {
        //adding tags into post
        let post = await PostModel.findOneAndUpdate({
            _id: req.body.postId
        }, {
            $push: {
                tags: req.body.tagName
            }
        })
        res.send({ status: true, message: 'saved successfully', data: saved})
    } else {
        res.send({ status: false, message: 'Not Saved'})
    }

  })
} catch (error) {
    console.log(error)
		return res.status(500).json({
			status: 'fail',
			message: error.message
		})
	}

}
