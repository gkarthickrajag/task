const { validationResult } = require('express-validator')
const PostModel = require('../models/postModel')
const  AWS = require('aws-sdk')


// add posts
exports.addPost = async (req, res) => {
    // validating the i/p parameters
    const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ success: false, message: 'Invalid Inputs', errors: errors.array(), code: 'INVALID_INPUT' })
  }
  try {
  const postJson = {
    title: req.body.title,
    description: req.body.description
  }
  //instance created to save posts
  const posts = new PostModel(postJson)

  //save posts
   const saved = posts.save()
    if (saved) {
        res.send({ status: true, message: 'saved successfully'})
    } else {
        res.send({ status: false, message: 'Not Saved'})
    }
} catch (error) {
    console.log(error)
		return res.status(500).json({
			status: 'fail',
			message: error.message
		})
	}
}

// list posts with sorting, filter, pagination
exports.getPosts = async (req, res) => {
    try {
        // console.log('entry')
        let skip = 0
        //query limit will get from params
        let limit = req.query.limit
        //page will get from from params
        let page = req.query.page
        if (page != 1) {
            skip = (page - 1) * 10
        }
                
        // filter query
        let query = { active: req.query.status}

         // find total number of records
         const total = await PostModel.find(query).count()

        //find query retrives post data, with filter query 
        await PostModel.find(query,function (err, found){
            if(err) {
                res.send({ status: false, message: err.message})
            } else if (found.length>0) {
                //pagination added here with total, current_page, next_page, prev_page
                res.send({status: true, message: 'posts found', data: found, total, limit: limit, current_page: page, next_page: parseInt(page) + 1, prev_page: (page === 1) ? 1 : page - 1 })
            } else {
                //pagination added here with total, current_page, next_page, prev_page
                res.send({status: true, message: 'No posts found', data: [], total, limit: 10, current_page: page, next_page: parseInt(page) + 1, prev_page: (page === 1) ? 1 : page - 1 })
            }
        }).skip(skip).limit(limit).sort({createdAt:-1})
    } catch (error) {
            console.log(error)
		    return res.status(500).json({
			status: 'fail',
			message: error.message
		})
	}

}

// saerchPosts
exports.searchPosts = async (req, res) => {
    try{
        let skip = 0
        //query limit will get from params
        let limit = req.query.limit
        //page will get from from params
        let page = req.query.page
        if (page != 1) {
            skip = (page - 1) * 10
        }
        //query to search key 
        const query = {
            $or: [{
              title: { $regex: `.*${searchTerm}.*`, $options: 'i' }
            }, {
              description: { $regex: `.*${searchTerm}.*`, $options: 'i' }
            }]
          }

          // find total number of records
         const total =  await PostModel.find(query).count()

          await PostModel.find(query,function (err, found){
            if(err) {
                res.send({ status: false, message: err.message})
            } else if (found.length>0) {
                //pagination added here with total, current_page, next_page, prev_page
                res.send({status: true, message: 'posts found', data: found, total, limit: limit, current_page: page, next_page: parseInt(page) + 1, prev_page: (page === 1) ? 1 : page - 1 })
            } else {
                //pagination added here with total, current_page, next_page, prev_page
                res.send({status: true, message: 'No posts found', data: [], total, limit: 10, current_page: page, next_page: parseInt(page) + 1, prev_page: (page === 1) ? 1 : page - 1 })
            }
        }).skip(skip).limit(limit).sort({createdAt:-1})

    } catch (error) {
        console.log(error)
        return res.status(500).json({
        status: 'fail',
        message: error.message
    })
}
}

exports.filterByTag = async(req, res) => {
        try {
            let skip = 0
            //query limit will get from params
            let limit = req.query.limit
            //page will get from from params
            let page = req.query.page
            if (page != 1) {
                skip = (page - 1) * 10
            }
                    
            // filter query
            let query = { tags: { $in: req.body.tags}}
    
             // find total number of records
             const total =  PostModel.find(query).count()
    
            //find query retrives post data, with filter query 
            await PostModel.find(query,function (err, found){
                if(err) {
                    res.send({ status: false, message: err.message})
                } else if (found.length>0) {
                    //pagination added here with total, current_page, next_page, prev_page
                    res.send({status: true, message: 'posts found', data: found, total, limit: limit, current_page: page, next_page: parseInt(page) + 1, prev_page: (page === 1) ? 1 : page - 1 })
                } else {
                    //pagination added here with total, current_page, next_page, prev_page
                    res.send({status: true, message: 'No posts found', data: [], total, limit: 10, current_page: page, next_page: parseInt(page) + 1, prev_page: (page === 1) ? 1 : page - 1 })
                }
            }).skip(skip).limit(limit).sort({createdAt:-1})
        } catch (error) {
                console.log(error)
                return res.status(500).json({
                status: 'fail',
                message: error.message
            })
        }
    
    }

exports.uploadImage = async (req, res) => {
    const s3 = new AWS.S3({
        accessKeyId: config.AWS_S3_ACCESS_KEY_ID,
        secretAccessKey: config.AWS_S3_SECRET_ACCESS_KEY,
      })
    const imagePath = req.files[0].path
    const blob = fs.readFileSync(imagePath)
    const uploadedImage = await s3.upload({
        Bucket: config.AWS_S3_BUCKET_NAME,
        Key: req.files[0].originalFilename,
        Body: blob,
      }).promise()

    if(uploadedImage) {
        res.send({ status: true, message: 'Image uploaded susccesfully', imageurl:uploadedImage.location})
    } else {
        res.send({ status: false, message: 'Image not uploaded'})
    }
      
}
